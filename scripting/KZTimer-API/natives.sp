/*
	KZTimer API Helper ~ Natives
*/

void CreateNatives()
{
	CreateNative("KZTimerAPI_PrintToChat", Native_PrintToChat);
	CreateNative("KZTimerAPI_PrintMapTier", Native_PrintMapTier);
	CreateNative("KZTimerAPI_PrintGlobalRecord", Native_PrintGlobalRecord);
	CreateNative("KZTimerAPI_PrintGlobalRecordTop", Native_PrintGlobalRecordTop);
	CreateNative("KZTimerAPI_PrintGlobalRecordTopMenu", Native_PrintGlobalRecordTopMenu);
	CreateNative("KZTimerAPI_PrintGlobalJumpstatsTopMenu", Native_PrintGlobalJumpstatsTopMenu);
	CreateNative("KZTimerAPI_InsertRecord", Native_InsertRecord);
	CreateNative("KZTimerAPI_InsertJumpstat", Native_InsertJumpstat);
	CreateNative("KZTimerAPI_InsertGlobalBan", Native_InsertGlobalBan);
	CreateNative("KZTimerAPI_GlobalCheck", Native_GlobalCheck);
}

// =========================  NATIVES  ========================= //

// Taken from GOKZ, thanks DanZay
// https://bitbucket.org/kztimerglobalteam/gokz
public int Native_PrintToChat(Handle plugin, int numParams)
{
	int client = GetNativeCell(1);
	bool addPrefix = GetNativeCell(2);
	
	char buffer[1024];
	SetGlobalTransTarget(client);
	FormatNativeString(0, 3, 4, sizeof(buffer), _, buffer);

	if (addPrefix)
	{
		char prefix[64];
		FormatEx(prefix, sizeof(prefix), "[{lightgreen}KZ-API{default}]");
		Format(buffer, sizeof(buffer), "%s %s", prefix, buffer);
	}

	CPrintToChat(client, "%s", buffer);
}

public int Native_InsertRecord(Handle plugin, int numParams)
{
	int client = GetNativeCell(1);
	int teleports = GetNativeCell(2);
	float time = GetNativeCell(3);
	
	InsertTime(GetClientUserId(client), teleports, time);
}

public int Native_InsertJumpstat(Handle plugin, int numParams)
{
	int client = GetNativeCell(1);
	APIJumptype jumptype = GetNativeCell(2);
	int jumpcolor = GetNativeCell(3);
	float distance = GetNativeCell(4);
	bool personalbest = GetNativeCell(5);
	
	InsertJump(GetClientUserId(client), jumptype, jumpcolor, distance, personalbest);
}

public int Native_PrintMapTier(Handle plugin, int numParams)
{
	int client = GetNativeCell(1);

	PrintMapTier(GetClientUserId(client));
}

public int Native_PrintGlobalRecord(Handle plugin, int numParams)
{
	int client = GetNativeCell(1);

	PrintGlobalRecord(GetClientUserId(client));
}

public int Native_PrintGlobalRecordTop(Handle plugin, int numParams)
{
	char mapName[128];
	char runType[30];
	
	int client = GetNativeCell(1);
	GetNativeString(2, mapName, sizeof(mapName));
	GetNativeString(3, runType, sizeof(runType));
	int tickrate = GetNativeCell(4);

	PrintGlobalRecordTop(GetClientUserId(client), mapName, runType, tickrate);
}

public int Native_PrintGlobalRecordTopMenu(Handle plugin, int numParams)
{
	int client = GetNativeCell(1);
	SetSelectedMap(client, "");
	
	DisplayRecordTopType(client);
}

public int Native_PrintGlobalJumpstatsTopMenu(Handle plugin, int numParams)
{
	int client = GetNativeCell(1);
	
	DisplayJumpstatsBindSelect(client);
}

public int Native_InsertGlobalBan(Handle plugin, int numParams)
{
	int client = GetNativeCell(1);

	char banType[64];
	char banNotes[1000];
	char banStats[1000];

	GetNativeString(2, banType, sizeof(banType));
	GetNativeString(3, banNotes, sizeof(banNotes));
	GetNativeString(4, banStats, sizeof(banStats));

	InsertGlobalBan(GetClientUserId(client), banType, banNotes, banStats);
}

public int Native_GlobalCheck(Handle plugin, int numParams)
{
	int client = GetNativeCell(1);
	
	GlobalCheck(GetClientUserId(client));
}
