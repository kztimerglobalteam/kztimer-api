/*
	KZTimer API Helper - Convars
*/

ConVar gCV_broadcastedPlace;
ConVar gCV_globaltopCount;
ConVar gCV_js_broadcastedPlace;
ConVar gCV_js_topCount;

void CreateConVars()
{
	gCV_broadcastedPlace = CreateConVar("KZAPI_broadcasted_place", "20", "Mininum place to be broadcasted", _, true, 1.0, true, 100.0);
	gCV_globaltopCount = CreateConVar("KZAPI_globaltop_count", "20", "Value on how many global records to show on the global top", _, true, 5.0, true, 100.0);
	
	gCV_js_broadcastedPlace = CreateConVar("KZAPI_js_broadcasted_place", "20", "Mininum place to be broadcasted (Jumpstats)", _, true, 1.0, true, 30.0);
	gCV_js_topCount = CreateConVar("KZAPI_js_top_count", "20", "Value on how many jumpstats to show on the global jumpstats top", _, true, 5.0, true, 30.0);

	AddChangeHooks();
}

void AddChangeHooks()
{
	gCV_broadcastedPlace.AddChangeHook(OnBroadcastedPlace_Changed);
	gCV_globaltopCount.AddChangeHook(OnGlobaltopCount_Changed);
	gCV_js_broadcastedPlace.AddChangeHook(OnJSbroadcastedPlace_Changed);
	gCV_js_topCount.AddChangeHook(OnJStopCount_Changed);
}