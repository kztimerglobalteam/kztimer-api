/*
	KZTimer API Helper ~ Callbacks - Insert Jump
*/

public int InsertJumpstatCallback(bool bFailure, int id, int place, DataPack dp)
{
	dp.Reset();
	int client = GetClientOfUserId(dp.ReadCell());
	int jumptype = dp.ReadCell();
	delete dp;

	if (!bFailure)
	{
		PrintToConsole(client, "[KZ-API] Jump has been registered! [ID: %d]", id);

		if (place == 1)
		{
			KZTimerAPI_PrintToChatAll(true, "%t", "New Jumpstat Record", client, gC_JumpTypes[jumptype]);
			EmitSoundToAllAny(RECORD_SOUND);
		}

		else if (place <= gCV_js_broadcastedPlace.IntValue && place != 0)
		{
			KZTimerAPI_PrintToChatAll(true, "%t", "New Jumpstat Top Record", client, place, gC_JumpTypes[jumptype]);
		}
	}
}