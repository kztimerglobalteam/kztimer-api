/*
	KZTimer API Helper ~ Callbacks - Jumpstats Top
*/

// TODO This is here until I decide where its supposed to go
public void DisplayJumpInfo(int client, const char[] jumpInfo)
{
	APIJumpRecord APIJump = new APIJumpRecord(jumpInfo);
	
	int jumpId = APIJump.ID();
	int serverId = APIJump.ServerID();
	int jumpType = APIJump.JumpType();
	int tickRate = APIJump.Tickrate();
	int strafes = APIJump.Strafes();
	float distance = APIJump.Distance();

	char playerName[128];
	APIJump.PlayerName(playerName, sizeof(playerName));

	char createdOn[60];
	APIJump.CreatedOn(createdOn, sizeof(createdOn));
	FormatISO8601DateTime(createdOn, createdOn, sizeof(createdOn));
	
	char steamId[40];
	APIJump.SteamID(steamId, sizeof(steamId));
	
	char steamId64[30];
	GetCommunityID(steamId, steamId64, sizeof(steamId64));

	PrintToConsole(client, "\n- Player \n-- Name: %s \n-- SteamID: %s \n-- SteamID64: %s\n", playerName, steamId, steamId64);
	PrintToConsole(client, "- Server \n-- ID: %d \n-- Tickrate: %d\n", serverId, tickRate);
	PrintToConsole(client, "- Jump \n-- ID: %d \n-- Type: %s", jumpId, gC_JumpTypes[jumpType]);
	PrintToConsole(client, "-- Distance: %f \n-- Strafes: %d \n-- Timestamp: %s UTC\n", distance, strafes, createdOn);
	PrintToConsole(client, "- Profile Link: %s%s\n", "https://steamcommunity.com/profiles/", steamId64);
	
	delete APIJump;
}

public int JumpstatsTopCallback(bool bFailure, const char[] top, DataPack dp)
{
	char bindString[40];

	dp.Reset();
	int client = GetClientOfUserId(dp.ReadCell());
	APIJumptype jumpType = dp.ReadCell();
	dp.ReadString(bindString, sizeof(bindString));
	delete dp;

	float distance;
	int place, strafes;
	char buffer[2048], display[300];
	char playerName[128], playerSteamId[40];
	
	if (bFailure)
	{
		KZTimerAPI_PrintToChat(client, true, "%t", "No Jumpstats Found", gC_JumpTypes[jumpType], bindString); // Make something else for this
	}
	
	else
	{
		Menu menu = new Menu(MenuHandler_JumpstatsTop);
		
		APIJumpRecordList jumps = new APIJumpRecordList(top);
	
		if (jumps.Count() <= 0)
		{
			KZTimerAPI_PrintToChat(client, true, "%t", "No Jumpstats Found", gC_JumpTypes[jumpType], bindString);
			delete menu;
			delete jumps;
			return;
		}
	
		for (int i = 0; i < gCV_js_topCount.IntValue; i++)
		{
			if (jumps.Count() == i)
			{
				break;
			}
		
			jumps.GetByIndex(i, buffer, sizeof(buffer));
			APIJumpRecord APIJump = new APIJumpRecord(buffer);

			APIJump.PlayerName(playerName, sizeof(playerName));
			APIJump.SteamID(playerSteamId, sizeof(playerSteamId));

			place = i + 1;
			strafes = APIJump.Strafes();
			distance = APIJump.Distance();
			
			if (StrEqual(playerName, ""))
			{
				// Replace name with SteamID incase empty name
				APIJump.SteamID(playerName, sizeof(playerName));
			}
			
			// Sorry to anyone reading these
			if (i < 9)
			{
				if (strafes <= 9)
				{
					Format(display, sizeof(display), "[%d]%7s%.3f%10s%d%9s%s", place, "   |", distance, "    |", strafes, "    |", playerName);
				}
				else
				{
					Format(display, sizeof(display), "[%d]%7s%.3f%9s%d%8s%s", place, "   |", distance, "    |", strafes, "   |", playerName);
				}
			}
			
			
			else if (i >= 9)
			{
				if (strafes <= 9)
				{
					Format(display, sizeof(display), "[%d]%5s%.3f%10s%d%9s%s", place, " |", distance, "    |", strafes, "    |", playerName);
				}
				else
				{
					Format(display, sizeof(display), "[%d]%5s%.3f%9s%d%8s%s", place, " |", distance, "    |", strafes, "   |", playerName);
				}
			}

			menu.AddItem(buffer, display, ITEMDRAW_DEFAULT);
			delete APIJump;
		}

		delete jumps;

		KZTimer_StopUpdatingOfClimbersMenu(client);
		menu.SetTitle("%T\nRank%8sDistance%6sStrfs%8sPlayer", "Jumpstats Top Title", client, gC_JumpTypes[jumpType], bindString, "    |", "   |", "  |");
		menu.ExitBackButton = true;
		menu.Pagination = 5;
		menu.Display(client, MENU_TIME_FOREVER);
	}
}