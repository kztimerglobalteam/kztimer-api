/*
	KZTimer API Helper ~ Callbacks - Global Record Top
*/

public int PrintGlobalRecordTopCallback(bool bFailure, const char[] top, DataPack dp)
{
	char mode[64], map[128];

	dp.Reset();
	int client = GetClientOfUserId(dp.ReadCell());
	bool isPro = dp.ReadCell();
	dp.ReadString(mode, sizeof(mode));
	dp.ReadString(map, sizeof(map));
	int tickRate = dp.ReadCell();
	delete dp;

	SetSelectedMap(client, map);

	char playerName[256];
	char mapTime[128];
	char runType[40];
	char display[200];
	char buffer[1024];

	Format(runType, sizeof(runType), "%s", isPro ? "PRO" : "TP");

	if (bFailure)
	{
		if (isPro)
		{
			KZTimerAPI_PrintToChat(client, true, "%t", "No Records Found PRO", runType, map, tickRate);  // Replace with something else
		}
		else
		{
			KZTimerAPI_PrintToChat(client, true, "%t", "No Records Found TP", runType, map, tickRate); // Replace with something else
		}		
	}

	else
	{
		Menu menu = new Menu(MenuHandler_RecordTop);
		
		APIRecordList records = new APIRecordList(top);

		if (records.Count() <= 0)
		{
			if (isPro)
			{
				KZTimerAPI_PrintToChat(client, true, "%t", "No Records Found PRO",  runType, map, tickRate);
			}
			else
			{
				KZTimerAPI_PrintToChat(client, true, "%t", "No Records Found TP", runType, map, tickRate);
			}

			delete menu;
			delete records;
			return;
		}
		
		char title[100];
		FormatEx(title, sizeof(title), " Rank%9sTime%21sPlayer", "    |", "               |");
		
		//menu.AddItem("", title, ITEMDRAW_DISABLED);
		
		for (int i = 0; i < gCV_globaltopCount.IntValue; i++)
		{
			if (records.Count() == i)
			{
				break;
			}

			records.GetByIndex(i, buffer, sizeof(buffer));
			APIRecord record = new APIRecord(buffer);

			record.PlayerName(playerName, sizeof(playerName));

			if (StrEqual(playerName, ""))
			{
				record.SteamID(playerName, sizeof(playerName));
			}

			Format(mapTime, sizeof(mapTime), "%s", FormatRecordTime(record.Time()));

			if (i < 9)
			{
				Format(display, sizeof(display), " [0%d]%10s[%s]%11s%s", i + 1, "     |", mapTime, "     |", playerName);
			}

			else
			{
				Format(display, sizeof(display), " [%d]%10s[%s]%11s%s", i + 1, "     |", mapTime, "     |", playerName);
			}

			delete record;
			menu.AddItem("", display, ITEMDRAW_DEFAULT);
		}

		delete records;

		KZTimer_StopUpdatingOfClimbersMenu(client);
		menu.SetTitle("%T\nRank%14sTime%21sPlayer", "Global Top Title", client, mode, runType, map, gI_tickRate, "         |", "               |");
		menu.ExitBackButton = true;
		menu.Pagination = 5;
		menu.Display(client, MENU_TIME_FOREVER);
	}
}