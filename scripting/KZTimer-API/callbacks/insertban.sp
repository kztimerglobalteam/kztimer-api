/*
	KZTimer API Helper ~ Callbacks - Insert Ban
*/


public int InsertGlobalBanCallback(bool bFailure, int userid)
{
	int client = GetClientOfUserId(userid);
	
	char steamid[64];
	GetClientAuthId(client, AuthId_Steam2, steamid, sizeof(steamid));
	
	LogMessage("Succesfully banned %N (%s)", client, steamid);
}