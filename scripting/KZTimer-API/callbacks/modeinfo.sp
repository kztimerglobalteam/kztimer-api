/*
	KZTimer API Helper ~ Callbacks - Mode Info
*/

public int ModeInfoCallback(bool bFailure, const char[] name, int version, const char[] version_desc, any data)
{
	gB_ValidVersion = false;
	
	int KZTimer_Version = KZTimer_GetVersion();
	
	if (KZTimer_Version >= version)
	{
		gB_ValidVersion = true;
	}
}