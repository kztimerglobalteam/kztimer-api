/*
	KZTimer API Helper ~ Callbacks - Insert Time
*/

public int InsertTimeCallback(bool bFailure, int place, int top_place, int top_overall_place, DataPack dp)
{
	dp.Reset();
	float time = dp.ReadCell();
	int client = GetClientOfUserId(dp.ReadCell());
	bool teleports = dp.ReadCell();
	delete dp;
	
	PrintToConsole(client, "[KZ-API] Record Registered!");
	
	char timeString[64];
	Format(timeString, sizeof(timeString), "%s", FormatRecordTime(time));

	if (top_place == 1)
	{
		KZTimerAPI_PrintToChatAll(true, "%t", "New Global Record", client, teleports ? "TP" : "PRO");
		EmitSoundToAllAny(RECORD_SOUND);
	}

	else if (top_place <= gCV_broadcastedPlace.IntValue && top_place != 0)
	{
		KZTimerAPI_PrintToChatAll(true, "%t", "New Global Top Record", client, top_place, teleports ? "TP" : "PRO", gSZ_currentMap, timeString, gI_tickRate);
	}
}