/*
	KZTimer API Helper ~ Callbacks - Global Record PRO
*/

public int PrintGlobalRecordPROCallback(bool bFailure, const char[] top, DataPack dp)
{
	dp.Reset();
	int client = GetClientOfUserId(dp.ReadCell());
	delete dp;

	if (!bFailure)
	{
		APIRecordList records = new APIRecordList(top);

		if (records.Count() <= 0)
		{
			KZTimerAPI_PrintToChat(client, true, "%t", "No Pro Records");
			delete records;
			return;
		}

		char buffer[2048];
		records.GetByIndex(0, buffer, sizeof(buffer));
	
		APIRecord record = new APIRecord(buffer);

		char playerName[128];
		record.PlayerName(playerName, sizeof(playerName));
	
		char time[256];
		Format(time, sizeof(time), "%s", FormatRecordTime(record.Time()));
	
		KZTimerAPI_PrintToChat(client, true, "%t", "Pro Record", time, playerName);
	
		delete record;
		delete records;
	}
}