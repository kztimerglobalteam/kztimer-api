/*
	KZTimer API Helper ~ Menus - Jumpstats - Menus
*/

void DisplayJumpstatsSelect(int client)
{
	Menu menu = new Menu(MenuHandler_JumpstatsSelect);
	menu.SetTitle("%T", "Select Jumpstat", client);
	menu.AddItem("1", "Longjump");
	menu.AddItem("2", "Bhop");
	menu.AddItem("3", "Multihop");
	menu.AddItem("4", "Weirdjump");
	menu.AddItem("5", "Drophop");
	menu.AddItem("7", "Ladderjump");
	menu.AddItem("6", "Countjump");
	menu.ExitBackButton = true;
	menu.Display(client, MENU_TIME_FOREVER);
}

void DisplayJumpstatsBindSelect(int client)
{
	Menu menu = new Menu(MenuHandler_JumpstatsBindSelect);
	menu.SetTitle("%T", "Select Bindtype", client);
	menu.AddItem("Bind", "Bind");
	menu.AddItem("Nobind", "Nobind");
	menu.Display(client, MENU_TIME_FOREVER);
}