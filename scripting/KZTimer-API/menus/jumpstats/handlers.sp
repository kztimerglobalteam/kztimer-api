/*
	KZTimer API Helper ~ Menus - Jumpstats - Handlers
*/

public int MenuHandler_JumpstatsSelect(Menu menu, MenuAction action, int param1, int param2)
{
	if (action == MenuAction_End)
	{
		delete menu;
	}
	
	if (action == MenuAction_Select)
	{
		char info[64];
		menu.GetItem(param2, info, sizeof(info));
		APIJumptype jumpType = view_as<APIJumptype>(StringToInt(info));
		
		PrintGlobalJumpstatTop(GetClientUserId(param1), jumpType, gB_selectedBind[param1]);
	}

	if (action == MenuAction_Cancel && param2 == MenuCancel_ExitBack)
	{
		DisplayJumpstatsBindSelect(param1);
	}
}

public int MenuHandler_JumpstatsTop(Menu menu, MenuAction action, int param1, int param2)
{
	if (action == MenuAction_End)
	{
		delete menu;
	}
	
	if (action == MenuAction_Select)
	{
		char buffer[2048];
		menu.GetItem(param2, buffer, sizeof(buffer));
		
		DisplayJumpInfo(param1, buffer);
	}
	
	if (action == MenuAction_Cancel && param2 == MenuCancel_ExitBack)
	{
		DisplayJumpstatsSelect(param1);
	}
}

public int MenuHandler_JumpstatsBindSelect(Menu menu, MenuAction action, int param1, int param2)
{
	if (action == MenuAction_End)
	{
		delete menu;
	}
	
	if (action == MenuAction_Select)
	{
		char info[64];
		menu.GetItem(param2, info, sizeof(info));
		if (StrEqual(info, "Bind"))
		{
			gB_selectedBind[param1] = true;
		}
		else
		{
			gB_selectedBind[param1] = false;
		}

		DisplayJumpstatsSelect(param1);
	}
}