/*
	KZTimer API Helper ~ Menus - Global Top - Menus
*/

void DisplayRecordTopType(int client)
{
	Menu menu = new Menu(MenuHandler_RecordTopType);
	menu.SetTitle("%T", "Run Type Menu Title", client);
	menu.AddItem("", "PRO");
	menu.AddItem("", "TP");
	menu.Display(client, MENU_TIME_FOREVER);
}