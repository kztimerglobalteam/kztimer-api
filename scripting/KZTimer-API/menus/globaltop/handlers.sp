/*
	KZTimer API Helper ~ Menus - Global Top - Handlers
*/

public int MenuHandler_RecordTop(Menu menu, MenuAction action, int param1, int param2)
{
	if (action == MenuAction_End)
	{
		delete menu;
	}

	if (action == MenuAction_Cancel && param2 == MenuCancel_ExitBack)
	{
		DisplayRecordTopType(param1);
	}
}

public int MenuHandler_RecordTopType(Menu menu, MenuAction action, int param1, int param2)
{
	if (action == MenuAction_End)
	{
		delete menu;
	}
	
	if (action == MenuAction_Select)
	{
		char runType[64];
		switch (param2)
		{
			case 0:FormatEx(runType, sizeof(runType), "PRO");
			case 1:FormatEx(runType, sizeof(runType), "TP");
		}
		KZTimerAPI_PrintGlobalRecordTop(param1, gSZ_selectedMapName[param1], runType, gI_tickRate);
	}
}