/*
	KZTimer API Helper ~ Forwards - Jumpstats
*/

public int KZTimer_OnJumpstatCompleted(int client, int jumptype, int jumpcolor, float distance, bool personalbest)
{
	KZTimerAPI_InsertJumpstat(client, jumptype, jumpcolor, distance, personalbest);
}