/*
	KZTimer API Helper ~ Forwards - Kick If banned
*/

public void GlobalAPI_OnKickIfBanned_Changed(bool KickIfBanned)
{
	if (!KickIfBanned)
	{
		SetFailState("GlobalAPI_KickIfBanned is set to 0");
	}
}