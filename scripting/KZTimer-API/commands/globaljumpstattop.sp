/*
	KZTimer API Helper ~ Commands - Jumpstats Global Top
*/

public void PrintGlobalJumpstatTop(int userid, APIJumptype jumpType, bool bind)
{
	DataPack dp = CreateDataPack();
	dp.WriteCell(userid);
	dp.WriteCell(jumpType);
	dp.WriteString(bind ? "Binded" : "Nobind");

	GlobalAPI_GetJumpstatTopEx(jumpType, bind, JumpstatsTopCallback, dp);
}