/*
	KZTimer API Helper ~ Commands - Global Record
*/

public void PrintGlobalRecord(int userid)
{
	DataPack dp = CreateDataPack();
	dp.WriteCell(userid);


	DataPack dp2 = view_as<DataPack>(CloneHandle(dp));

	GlobalAPI_GetRecordTop(gSZ_currentMap, 0, GlobalMode_KZTimer, true, gI_tickRate, 1, PrintGlobalRecordPROCallback, dp);
	GlobalAPI_GetRecordTop(gSZ_currentMap, 0, GlobalMode_KZTimer, false, gI_tickRate, 1, PrintGlobalRecordTPCallback, dp2);
}