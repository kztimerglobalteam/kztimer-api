/*
	KZTimer API Helper ~ Commands - Global Check
*/

public void GlobalCheck(int userid)
{
	int client = GetClientOfUserId(userid);
	
	KZTimerAPI_PrintToChat(client, true, "%t", "Global Check",
	gB_ValidAPIKey ? "{green}✓{default}" : "{darkred}X{default}",
	gB_ValidVersion ? "{green}✓{default}" : "{darkred}X{default}",
	SettingCheck() ? "{green}✓{default}" : "{darkred}X{default}",
	MapCheck() ? "{green}✓{default}" : "{darkred}X{default}");
}