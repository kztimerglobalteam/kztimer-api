/*
	KZTimer API Helper ~ Commands - Insert Jump
*/

public void InsertJump(int userid, APIJumptype jumptype, int jumpcolor, float distance, bool personalbest)
{
	DataPack dp = CreateDataPack();
	dp.WriteCell(userid);
	dp.WriteCell(jumptype);

	GlobalAPI_SendJumpstat(GetClientOfUserId(userid), jumptype, jumpcolor, distance, personalbest, InsertJumpstatCallback, dp);
}