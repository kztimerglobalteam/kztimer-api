/*
	KZTimer API Helper ~ Commands - Map Tier
*/

public void PrintMapTier(int userid)
{
	int client = GetClientOfUserId(userid);
	int mapTier = GlobalAPI_GetMapTier();

	if (mapTier < 0 || mapTier > sizeof(gC_TierPhrases))
	{
		char buf[64];
		Format(buf, sizeof(buf), "Unknown (%d)", mapTier);

		KZTimerAPI_PrintToChat(client, true, "%t", "Map Tier", buf);
	}
	else
	{
		KZTimerAPI_PrintToChat(client, true, "%t", "Map Tier", gC_TierPhrases[mapTier]);
	}
}