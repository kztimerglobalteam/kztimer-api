/*
	KZTimer API Helper ~ Commands - Insert Time
*/

public void InsertTime(int userid, int teleports, float time)
{
	if (MapCheck())
	{
		int client = GetClientOfUserId(userid);
		char steamid[32];
	
		GetClientAuthId(client, AuthId_Steam2, steamid, sizeof(steamid));
	
		DataPack dp = CreateDataPack();
		dp.WriteCell(time);
		dp.WriteCell(userid);
		dp.WriteCell(view_as<bool>(teleports));
	
		GlobalAPI_SendRecord(client, GlobalMode_KZTimer, 0, teleports, time, InsertTimeCallback, dp);
	}
}