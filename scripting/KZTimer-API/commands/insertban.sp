/*
	KZTimer API Helper ~ Commands - Insert Ban
*/

public void InsertGlobalBan(int userid, char[] banType, char[] banNotes, char[] banStats)
{
	int client = GetClientOfUserId(userid);
	
	GlobalAPI_BanPlayer(client, banType, banNotes, banStats, InsertGlobalBanCallback, userid);
}