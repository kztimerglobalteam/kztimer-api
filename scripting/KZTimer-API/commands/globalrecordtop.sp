/*
	KZTimer API Helper ~ Commands - Global Record Top
*/

public void PrintGlobalRecordTop(int userid, char[] mapName, char[] runType, int tickRate)
{
	bool isPro = StrEqual(runType, "pro", false);
	
	if (tickRate == -1)
	{
		tickRate = GlobalAPI_GetTickrate();
	}
	
	if (StrEqual(mapName, ""))
	{
		FormatEx(mapName, 128, "%s", gSZ_currentMap);
	}

	DataPack dp = CreateDataPack();
	dp.WriteCell(userid);
	dp.WriteCell(isPro);
	dp.WriteString("KZTimer");
	dp.WriteString(mapName);
	dp.WriteCell(tickRate);

	GlobalAPI_GetRecordTop(mapName, 0, GlobalMode_KZTimer, isPro, tickRate, gCV_globaltopCount.IntValue, PrintGlobalRecordTopCallback, dp);
}