/*
	KZTimer API Helper - Convars ~ Global Top Count
*/

void OnGlobaltopCount_Changed(ConVar conVar, const char[] oldValue, const char[] newValue)
{
	if (!StrEqual(oldValue, newValue))
	{
		if (conVar == gCV_globaltopCount)
		{
			gCV_globaltopCount.IntValue = StringToInt(newValue);
		}
	}
}