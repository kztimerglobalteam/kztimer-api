/*
	KZTimer API Helper - Convars ~ Global Top Count
*/

void OnJSbroadcastedPlace_Changed(ConVar conVar, const char[] oldValue, const char[] newValue)
{
	if (!StrEqual(oldValue, newValue))
	{
		if (conVar == gCV_js_broadcastedPlace)
		{
			gCV_js_broadcastedPlace.IntValue = StringToInt(newValue);
		}
	}
}