/*
	KZTimer API Helper - Convars ~ Broadcasted Place
*/

void OnBroadcastedPlace_Changed(ConVar conVar, const char[] oldValue, const char[] newValue)
{
	if (!StrEqual(oldValue, newValue))
	{
		if (conVar == gCV_broadcastedPlace)
		{
			gCV_broadcastedPlace.IntValue = StringToInt(newValue);
		}
	}
}