/*
	KZTimer API Helper - Convars ~ Jumpstats Top Count
*/

void OnJStopCount_Changed(ConVar conVar, const char[] oldValue, const char[] newValue)
{
	if (!StrEqual(oldValue, newValue))
	{
		if (conVar == gCV_js_topCount)
		{
			gCV_js_topCount.IntValue = StringToInt(newValue);
		}
	}
}