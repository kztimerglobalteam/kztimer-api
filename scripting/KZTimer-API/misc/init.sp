/*
	KZTimer API Helper ~ Misc - Init
*/

void InitServer()
{
	GlobalAPI API;
	
	API.GetMapName(gSZ_currentMap, sizeof(gSZ_currentMap));
	API.GetMapPath(gSZ_mapPath, sizeof(gSZ_mapPath));
	gI_tickRate = API.GetTickrate();
	
	API.GetAuthStatus(AuthStatusCallback);
	API.GetModeInfo(GlobalMode_KZTimer, ModeInfoCallback);
}