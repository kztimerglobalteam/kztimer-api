/*
	KZTimer API Helper ~ Misc - Defaults
*/

void SetDefaults()
{
	gI_tickRate = -1;
	gB_ValidVersion = false;
	gB_ValidAPIKey = false;
	
	FormatEx(gSZ_currentMap, sizeof(gSZ_currentMap), "");
	FormatEx(gSZ_mapPath, sizeof(gSZ_mapPath), "");
}