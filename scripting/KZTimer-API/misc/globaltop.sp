/*
	KZTimer API Helper ~ Misc - Global Top
*/

void SetSelectedMap(int client, const char[] map)
{
	FormatEx(gSZ_selectedMapName[client], sizeof(gSZ_selectedMapName[]), map);
}