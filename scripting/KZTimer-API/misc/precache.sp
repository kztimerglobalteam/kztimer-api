/*
	KZTimer API Helper ~ Misc - Precache
*/

void PrecacheSounds()
{
	char soundPath[PLATFORM_MAX_PATH];
	FormatEx(soundPath, sizeof(soundPath), "sound/%s", RECORD_SOUND);

	AddFileToDownloadsTable(soundPath);
	PrecacheSoundAny(RECORD_SOUND);
}