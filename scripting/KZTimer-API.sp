/*
	KZTimer-API
	
	Provides API Natives to replace KZTimer's SQL system
*/

#include <GlobalAPI-Core>
#include <GlobalAPI-Jumpstats>

// ======================= DEFINITIONS ====================== //

#define UPDATER "updater.kztimerglobal.com/KZTimer-API.txt"
#define RECORD_SOUND "quake/holyshit_new.mp3"

// ======================= VARIABLES ======================= //

//Global Check
bool gB_ValidVersion = false;
bool gB_ValidAPIKey = false;
char gSZ_mapPath[PLATFORM_MAX_PATH] = "";

//Server Info
int gI_tickRate = -1;

//Map Info
char gSZ_currentMap[128] = "";
char gSZ_selectedMapName[MAXPLAYERS + 1][128];
bool gB_selectedBind[MAXPLAYERS + 1];

// ======================= PHRASES ======================= //

char gC_TierPhrases[][] =
{
	"None",
	"{lightgreen}Very Easy",
	"{green}Easy",
	"{yellow}Medium",
	"{lightred}Hard",
	"{red}Very Hard",
	"{darkred}Extreme",
	"{orchid}Death"
};

char gC_JumpTypes[APIJumptype_Count][] =
{
	"None",
	"Longjump",
	"Bhop",
	"Multihop",
	"Weirdjump",
	"Drophop",
	"Countjump",
	"Ladderjump"
};

// ======================= INCLUDES ======================= //

#undef REQUIRE_PLUGIN
#include <updater>

#define REQUIRE_PLUGIN

#include <cstrike>
#include <sdktools>
#include <sourcemod>
#include <emitsoundany>
#include <colorvariables>

#include <KZTimer>
#include <KZTimer-API>

#include "KZTimer-API/natives.sp"
#include "KZTimer-API/convars.sp"

#include "KZTimer-API/forwards/apikey.sp"
#include "KZTimer-API/forwards/jumpstats.sp"
#include "KZTimer-API/forwards/kickifbanned.sp"

#include "KZTimer-API/convars/place.sp"
#include "KZTimer-API/convars/jump_top.sp"
#include "KZTimer-API/convars/globaltop.sp"
#include "KZTimer-API/convars/jump_place.sp"

#include "KZTimer-API/menus/globaltop/menus.sp"
#include "KZTimer-API/menus/jumpstats/menus.sp"
#include "KZTimer-API/menus/globaltop/handlers.sp"
#include "KZTimer-API/menus/jumpstats/handlers.sp"

#include "KZTimer-API/misc/init.sp"
#include "KZTimer-API/misc/precache.sp"
#include "KZTimer-API/misc/defaults.sp"
#include "KZTimer-API/misc/mapcheck.sp"
#include "KZTimer-API/misc/globaltop.sp"
#include "KZTimer-API/misc/settingcheck.sp"

#include "KZTimer-API/commands/maptier.sp"
#include "KZTimer-API/commands/insertban.sp"
#include "KZTimer-API/commands/inserttime.sp"
#include "KZTimer-API/commands/insertjump.sp"
#include "KZTimer-API/commands/globalcheck.sp"
#include "KZTimer-API/commands/globalrecord.sp"
#include "KZTimer-API/commands/globalrecordtop.sp"
#include "KZTimer-API/commands/globaljumpstattop.sp"

#include "KZTimer-API/callbacks/modeinfo.sp"
#include "KZTimer-API/callbacks/insertban.sp"
#include "KZTimer-API/callbacks/authstatus.sp"
#include "KZTimer-API/callbacks/inserttime.sp"
#include "KZTimer-API/callbacks/insertjump.sp"
#include "KZTimer-API/callbacks/globalrecordtp.sp"
#include "KZTimer-API/callbacks/globalrecordtop.sp"
#include "KZTimer-API/callbacks/globalrecordpro.sp"
#include "KZTimer-API/callbacks/globaljumpstattop.sp"

// ======================= FORMATTING ====================== //

#pragma semicolon 1
#pragma newdecls required
#pragma dynamic 131072

// ======================= MAIN CODE ======================= //

public Plugin myinfo = 
{
	name = "KZTimer-API",
	author = "Sikari",
	description = "Plugin that provides natives to KZTimer to use the API",
	version = KZTimerAPI_Version,
	url = ""
};

public APLRes AskPluginLoad2(Handle myself, bool late, char[] error, int err_max)
{
	RegPluginLibrary("KZTimer-API");
	
	CreateNatives();
	CreateConVars();

	return APLRes_Success;
}

public void OnPluginStart()
{
	if (GetEngineVersion() != Engine_CSGO)
	{
		SetFailState("This plugin is for CSGO only!");
	}

	AutoExecConfig(true, "KZTimer-API");
	LoadTranslations("KZTimer-API.phrases");
}

public void OnAllPluginsLoaded()
{
	if (LibraryExists("updater"))
	{
		Updater_AddPlugin(UPDATER);
	}
}

public void OnLibraryAdded(const char[] name)
{
	if (StrEqual(name, "updater"))
	{
		Updater_AddPlugin(UPDATER);
	}
}

public void OnLibraryRemoved(const char[] name)
{
	if (StrEqual(name, "updater"))
	{
		Updater_RemovePlugin();
	}
}

public void OnMapStart()
{
	PrecacheSounds();
}

public void OnConfigsExecuted()
{
	if (FindConVar("GlobalAPI_KickIfBanned").BoolValue == false)
	{
		SetFailState("GlobalAPI_KickIfBanned is set to 0");
	}

	SetDefaults();
	InitServer();
}

public void OnClientPutInServer(int client)
{
	SetSelectedMap(client, "");
}