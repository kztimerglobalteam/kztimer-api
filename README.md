# KZTimer-API

Powerful SourceMod plugin providing natives for KZTimer to utilize the GlobalAPI

### **Features** ###
* Configurable amount of players shown in records and jumpstats top
* Configurable top amount of players records & jumpstats broadcasted in chat
* All phrases are in the translations (This means you can modify them to your liking!)

### **Installation** ###

* Make sure you have your GlobalAPI-Core AND GlobalAPI-Jumpstats installed and setup
* Make sure you have your API Key setup in `cfg/sourcemod/globalrecords.cfg`
* Drop this package into `addons/sourcemod/`
* Automatically generated config can be found on `cfg/sourcemod/KZTimer-API.cfg`

### **Server Requirements** ###

* Sourcemod 1.8+
* [GlobalAPI Core](https://bitbucket.org/kztimerglobalteam/globalrecordssmplugin)
* [GlobalAPI Jumpstats](https://bitbucket.org/kztimerglobalteam/globalrecordssmplugin)
* **OPTIONAL** - [Updater](https://forums.alliedmods.net/showthread.php?t=169095) (Updates your plugins automatically)